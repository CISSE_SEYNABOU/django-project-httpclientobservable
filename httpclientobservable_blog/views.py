from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from rest_framework import generics, permissions, status
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status , generics , mixins
from rest_framework.views import APIView
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

from .models import Todo
from .serializer import *

def index(request):
    return HttpResponse("Bienvenue dans le blog.")

class TodoView(generics.CreateAPIView):
    @api_view(['GET', 'POST'])
    def todo_list(request):
            """
            List all todos, or create a new product.
            """

            if request.method == 'GET':
                list = Todo.objects.all()
                serializer = TodoSerializer(list, many=True)
                return JsonResponse(serializer.data, safe=False)

            elif request.method == 'POST':
                todo_data=JSONParser().parse(request)
                serializer = TodoSerializer(data=todo_data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                return HttpResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'DELETE':
               Todo.objects.all().delete()
               return HttpResponse(status=status.HTTP_204_NO_CONTENT)

class TodoDetailView(generics.CreateAPIView):
    @api_view(['GET', 'PUT', 'DELETE'])
    def todo_detail(request, pk):
            """
            Retrieve, update or delete a product instance.
            """
            try:
                todo = Todo.objects.get(pk=pk)
            except Todo.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)

            if request.method == 'GET':
                serializer = TodoSerializer(todo)
                return JsonResponse(serializer.data)

            elif request.method == 'PUT':
                todo_data=JSONParser().parse(request)
                serializer = TodoSerializer(todo,data=todo_data)
                if serializer.is_valid():
                    serializer.save()
                    return JsonResponse(serializer.data)
                return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            elif request.method == 'DELETE':
                todo.delete()
                return HttpResponse(status=status.HTTP_204_NO_CONTENT)

