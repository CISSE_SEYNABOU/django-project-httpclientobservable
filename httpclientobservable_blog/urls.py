﻿from django.urls import path
from django.conf.urls import url
from . import views
from rest_framework import routers

router =routers.DefaultRouter()

urlpatterns = [
    path('',views.index, name='index'),
    url('Todolist/$',views.TodoView.todo_list), # simple view
    url('Todolist/(?P<pk>[0-9]+)/$',views.TodoDetailView.todo_detail), # simple view
    ]