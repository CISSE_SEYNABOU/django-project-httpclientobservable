from django.apps import AppConfig


class HttpClientObservable_BlogConfig(AppConfig):
    name = 'httpclientobservable_blog'
